SUBROUTINE exx_setup( )
  !
  IMPLICIT NONE
  !
  ! ====================================================================
  ! INPUT VARIABLES
  !
  ! LOCAL VARIABLES
  INTEGER  :: np, npsp1, npsp2, npsp3, npsp4, np_tmp_1, np_tmp_2 
  REAL(DP) :: x,y,z, dq(3),dqs(3),dist, rcut_sp2, rcut_sp3
  INTEGER  :: i,j,k, ierr, tmp
  !
  real(dp) :: rcut_tmp(4), rcut_sort(4), large_r
  integer  :: n_tmp(4)
  integer  :: order_rcut(4)
  integer  :: idx_min
  integer  :: idx
  ! --------------------------------------------------------------------
  !
  rcut_tmp = (/ exx_ps_rcut_s, exx_ps_rcut_p, exx_me_rcut_s, exx_me_rcut_p /)
  n_tmp = (/ np_in_sp_s, np_in_sp_p, np_in_sp_me_s, np_in_sp_me_p /)
  large_r = maxval(rcut_tmp,1)*100.d0
  do idx = 1, 4
    idx_min           = minloc(rcut_tmp,1)
    rcut_sort(idx)    = rcut_tmp(idx_min)
    rcut_tmp(idx_min) = large_r
    order_rcut(idx)   = idx_min
  end do ! idx
  !
  !! This part would be necessary if number of points in PS and ME sphere
  !! change every step
  !IF(n_exx.GT.0) THEN 
  !    IF( ALLOCATED( odtothd_in_sp ) )  DEALLOCATE(odtothd_in_sp )
  !    IF( ALLOCATED( thdtood_in_sp ) )  DEALLOCATE(thdtood_in_sp )
  !    IF( ALLOCATED( thdtood  ))        DEALLOCATE(thdtood)
  !    IF( ALLOCATED( xx_in_sp ))        DEALLOCATE(xx_in_sp )
  !    IF( ALLOCATED( yy_in_sp ))        DEALLOCATE(yy_in_sp )
  !    IF( ALLOCATED( zz_in_sp ))        DEALLOCATE(zz_in_sp )
  !END IF
  !
  ALLOCATE( odtothd_in_sp(3, np_in_sp_me_s ), stat=ierr )
  ALLOCATE( thdtood_in_sp(nr1, nr2, nr3), stat=ierr )
  ALLOCATE( thdtood(nr1, nr2, nr3), stat=ierr )
  ALLOCATE( xx_in_sp(1:np_in_sp_me_s), stat=ierr )
  ALLOCATE( yy_in_sp(1:np_in_sp_me_s), stat=ierr )
  ALLOCATE( zz_in_sp(1:np_in_sp_me_s), stat=ierr )
  ALLOCATE( sc_xx_in_sp(1:np_in_sp_me_s), stat=ierr )
  ALLOCATE( sc_yy_in_sp(1:np_in_sp_me_s), stat=ierr )
  ALLOCATE( sc_zz_in_sp(1:np_in_sp_me_s), stat=ierr )
  !
  xx_in_sp=0.0_DP; sc_xx_in_sp=0.0_DP
  yy_in_sp=0.0_DP; sc_yy_in_sp=0.0_DP
  zz_in_sp=0.0_DP; sc_zz_in_sp=0.0_DP
  !
  thdtood_in_sp = 0; odtothd_in_sp = 0; thdtood = 0
  !
  !this is the cutoff acording to the size of sphere
  !
  rcut_sp2=MIN(exx_me_rcut_p,exx_ps_rcut_s)
  rcut_sp3=MAX(exx_me_rcut_p,exx_ps_rcut_s)
  np_tmp_1=MIN(np_in_sp_s,np_in_sp_me_p)
  np_tmp_2=MAX(np_in_sp_s,np_in_sp_me_p)
  np    = 0
  npsp1 = 0; npsp2 = 0; npsp3 = 0; npsp4 = 0
  !
  DO k = 1,nr3
    DO j = 1, nr2
      DO i =1, nr1
        np = np + 1
        thdtood(i,j,k) = np
        !
        ! distances between Grid points and center of the simulation cell in S space
        ! center of the box is set to grid point at int(nr1/2), int(nr2/2), int(nr3/2) for every cell
        ! 
        dqs(1) = (DBLE(i)/DBLE(nr1)) - DBLE(INT(nr1/2))/DBLE(nr1)
        dqs(2) = (DBLE(j)/DBLE(nr2)) - DBLE(INT(nr2/2))/DBLE(nr2)
        dqs(3) = (DBLE(k)/DBLE(nr3)) - DBLE(INT(nr3/2))/DBLE(nr3)
        !
        ! Here we are computing distances between Grid points and center of the simulation cell, so no MIC is needed ...
        ! Compute distance between grid point and the center of the simulation cell in R space 
        !
        dq(1)=h_in(1,1)*dqs(1)+h_in(1,2)*dqs(2)+h_in(1,3)*dqs(3)   !r_i = h s_i
        dq(2)=h_in(2,1)*dqs(1)+h_in(2,2)*dqs(2)+h_in(2,3)*dqs(3)   !r_i = h s_i
        dq(3)=h_in(3,1)*dqs(1)+h_in(3,2)*dqs(2)+h_in(3,3)*dqs(3)   !r_i = h s_i
        !
        dist = DSQRT(dq(1)*dq(1)+dq(2)*dq(2)+dq(3)*dq(3))
        !
        IF (dist .LE. rcut_sort(1)) THEN
          npsp1 = npsp1 + 1
          thdtood_in_sp(i,j,k)  = npsp1
          odtothd_in_sp(1,npsp1) = i
          odtothd_in_sp(2,npsp1) = j
          odtothd_in_sp(3,npsp1) = k
          !
          xx_in_sp(npsp1) = dq(1); sc_xx_in_sp(npsp1) = dqs(1)
          yy_in_sp(npsp1) = dq(2); sc_yy_in_sp(npsp1) = dqs(2)
          zz_in_sp(npsp1) = dq(3); sc_zz_in_sp(npsp1) = dqs(3)
          !
        ELSE IF (dist .LE. rcut_sort(2)) THEN
          !
          npsp2 = npsp2 + 1
          tmp = npsp2 + n_tmp(order_rcut(1))
          thdtood_in_sp(i,j,k)  = tmp
          odtothd_in_sp(1, tmp) = i
          odtothd_in_sp(2, tmp) = j
          odtothd_in_sp(3, tmp) = k
          !
          xx_in_sp(tmp) = dq(1); sc_xx_in_sp(tmp) = dqs(1)
          yy_in_sp(tmp) = dq(2); sc_yy_in_sp(tmp) = dqs(2)
          zz_in_sp(tmp) = dq(3); sc_zz_in_sp(tmp) = dqs(3)
          !
        ELSE IF (dist .LE. rcut_sort(3)) THEN
          !
          npsp3 = npsp3 + 1
          tmp = npsp3 + n_tmp(order_rcut(2))
          thdtood_in_sp(i,j,k)  = tmp
          odtothd_in_sp(1, tmp) = i
          odtothd_in_sp(2, tmp) = j
          odtothd_in_sp(3, tmp) = k
          !
          xx_in_sp(tmp) = dq(1); sc_xx_in_sp(tmp) = dqs(1)
          yy_in_sp(tmp) = dq(2); sc_yy_in_sp(tmp) = dqs(2)
          zz_in_sp(tmp) = dq(3); sc_zz_in_sp(tmp) = dqs(3)
          !
        ELSE IF (dist .LE. rcut_sort(4)) THEN
          !
          npsp4 = npsp4 + 1
          tmp = npsp4 + n_tmp(order_rcut(3))
          thdtood_in_sp(i,j,k)  = tmp
          odtothd_in_sp(1, tmp) = i
          odtothd_in_sp(2, tmp) = j
          odtothd_in_sp(3, tmp) = k
          !
          xx_in_sp(tmp) = dq(1); sc_xx_in_sp(tmp) = dqs(1)
          yy_in_sp(tmp) = dq(2); sc_yy_in_sp(tmp) = dqs(2)
          zz_in_sp(tmp) = dq(3); sc_zz_in_sp(tmp) = dqs(3)
          !
        END IF
      END DO
    END DO
  END DO
  !
  !IF ( npsp1 .NE. np_in_sp_p) THEN
  !  WRITE(stdout, *)&
  !    'number of points in the 1st shell does not match', npsp1, np_in_sp_p
  !  WRITE(stdout, *)'STOP in exx_setup'
  !  RETURN
  !END IF
  !!
  !IF ( npsp2 .NE. np_tmp_1-np_in_sp_p) THEN
  !  WRITE(stdout,*)&
  !    'number of points in the 2nd shell does not match', npsp2, np_tmp_1-np_in_sp_p
  !  WRITE(stdout, *)'STOP in exx_setup'
  !  RETURN
  !END IF
  !!
  !IF ( npsp3 .NE. np_tmp_2-np_tmp_1) THEN
  !  WRITE(stdout,*)&
  !    'number of points in the 3rd shell does not match', npsp3, np_tmp_2-np_tmp_1
  !  WRITE(stdout, *)'STOP in exx_setup'
  !  RETURN
  !END IF
  !!
  !IF ( npsp4 .NE. np_in_sp_me_s-np_tmp_2) THEN
  !  WRITE(stdout,*)&
  !    'number of points in the 4th shell does not match', npsp4, np_in_sp_me_s-np_tmp_2
  !  WRITE(stdout, *)'STOP in exx_setup'
  !  RETURN
  !END IF
  !
  RETURN
  !
END SUBROUTINE exx_setup
