  SUBROUTINE driver()
    USE io_global,              ONLY : stdout,ionode,ionode_id
    USE io_files,               ONLY : srvaddress,iunupdate
    USE mp_global,              ONLY : mp_startup,intra_image_comm,inter_bgrp_comm,nbgrp
    USE mp,                     ONLY : mp_bcast
    USE control_flags,          ONLY : conv_elec,thdyn,isave,tpre,iverbosity,nomore
    USE cp_main_variables,      ONLY : nfi,acc,lambda,lambdam,descla,rhor 
    USE uspp,                   ONLY : okvan,nlcc_any
    USE cell_base,              ONLY : alat,at,omega,h,hold,velh,ainv
    USE ions_positions,         ONLY : fion,tau0,taus,tausm,vels,velsm 
    USE energies,               ONLY : etot,ekincm,eself
    USE wavefunctions_module,   ONLY : c0_bgrp,cm_bgrp
    USE electrons_nose,         ONLY : xnhe0,xnhem,vnhe 
    USE ions_nose,              ONLY : xnhp0,xnhpm,vnhp,nhpcl,nhpdim 
    USE cell_nose,              ONLY : xnhh0,xnhhm,xnhhp,vnhh
    USE time_step,              ONLY : tps, delt
    USE ensemble_dft,           ONLY : z0t
    USE electrons_base,         ONLY : f
    USE cp_interfaces,          ONLY : writefile,newinit
    USE constants,              ONLY : au_gpa, au_ps

    IMPLICIT NONE
    
    INTEGER, PARAMETER :: MSGLEN=12
    LOGICAL :: isinit=.true., hasdata=.false., firststep=.true.
    CHARACTER*12 :: header
    CHARACTER*1024 :: parbuffer
    INTEGER socket, nat, flag
    INTEGER inet, port, ccmd, i, exst
    CHARACTER*1024 :: host  
    REAL*8 :: cellh(3,3), cellih(3,3), vir(3,3), pot
    REAL*8, ALLOCATABLE :: combuf(:)
    REAL*8 :: stress(3,3)=0.0d0 
    REAL*8 :: stress_gpa(3,3)=0.0d0 

    INTEGER  :: iter,ia 
    LOGICAL  :: tfirst,tlast
    REAL*8   :: enb,enbi,ccc,bigr

    !
    ! Inconsistencies with the present code ....
    !
    IF(okvan.OR.nlcc_any) CALL errore('driver','PI calculations are not working with okvan and nlcc_any...',1) 
    !
!   IF(thdyn) CALL errore('driver','PI calculations are not working with thdyn...',1) 
!   thdyn=.true.
!   tpre=.true.
    !
    flag=1
    tfirst = .TRUE. ! deal ..
    tlast  = .FALSE. ! deal .. 
    !
    inet=1
    host=srvaddress(1:INDEX(srvaddress,':',back=.true.)-1)//achar(0)
    read(srvaddress(INDEX(srvaddress,':',back=.true.)+1:),*) port
    
    IF (ionode) write(stdout,'(3X,"DRIVER MODE: Connecting to ",A)') trim(srvaddress)
    
    IF (srvaddress(1:INDEX(srvaddress,':')-1).eq.('UNIX')) THEN
      inet=0
      host=srvaddress(6:INDEX(srvaddress,':',back=.true.)-1)//achar(0)    
    ENDIF

    IF (ionode) call open_socket(socket, inet, port, host)
    !
    driver_loop: DO
      !
      CALL start_clock( 'driver_md' )
      !
      ! do communication on master node only...
      !
      if (ionode) call readbuffer(socket, header, MSGLEN, flag)      
      !
      call mp_bcast(flag,ionode_id,intra_image_comm)
      !
      IF(flag.EQ.0) GO TO 1 
      !
      call mp_bcast(header,ionode_id, intra_image_comm)
      ! 
      if (ionode) write(*,*) "  DRIVER MODE: Message from server: ", header
      !
      if (trim(header) == "STATUS") then
         !
         if (ionode) then  ! does not  need init (well, maybe it should, just to check atom numbers and the like... )
            if (hasdata) then
               call writebuffer(socket,"HAVEDATA    ",MSGLEN)
            else
               call writebuffer(socket,"READY       ",MSGLEN)
            endif
         endif
         !
      else if (trim(header) == "POSDATA") then              
         !
         if (ionode) then        
            call readbuffer(socket, cellh, 9*8, flag)
            call readbuffer(socket, cellih, 9*8, flag)
            call readbuffer(socket, nat, 4, flag)
         endif
         call mp_bcast(cellh,ionode_id, intra_image_comm)
         call mp_bcast(cellih,ionode_id, intra_image_comm)
         call mp_bcast(nat,ionode_id, intra_image_comm)
         !
         if (.not.allocated(combuf)) then
           allocate(combuf(3*nat))
         end if
         if (ionode) call readbuffer(socket, combuf, nat*3*8, flag)
         call mp_bcast(combuf,ionode_id, intra_image_comm)
         !
         if (ionode) write(*,*) "  DRIVER MODE: Received positions "
         if (firststep) then
            if (ionode) write(*,*) "  DRIVER MODE: Preparing first evaluation "
            call init_run()
            firststep=.false.
         end if 
         !
        !!DEBUG
        !DO ia=1,3
        !  WRITE(stdout,'(3x,"cell: ",I7,3F18.8)') nfi,h(:,ia) 
        !END DO
        !DO ia=1,3
        !  WRITE(stdout,'(3x,"ainv: ",I7,3F18.8)') nfi,ainv(:,ia) 
        !END DO
        !h=TRANSPOSE(cellh)
        !ainv=TRANSPOSE(cellih)
        !DO ia=1,3
        !  WRITE(stdout,'(3x,"h: ",I7,3F18.8)') nfi,h(:,ia) 
        !END DO
        !DO ia=1,3
        !  WRITE(stdout,'(3x,"celli: ",I7,3F18.8)') nfi,ainv(:,ia) 
        !END DO
        !!DEBUG
         !
         ! update number of steps, first step repeated in restart so skip ...
         !
         IF(.NOT.tfirst) nfi = nfi+1
         !
        !at=cellh/alat
         tau0 = RESHAPE(combuf, (/ 3 , nat /) )             
         !
         !DO ia=1,nat
         !  WRITE(stdout,'(3x,"tau0: ",I7,3F18.10)') nfi,tau0(:,ia) 
         !END DO
         !
         IF ( thdyn ) THEN
            !
            h=TRANSPOSE(cellh)
            !
            IF( nbgrp > 1 ) THEN
               CALL mp_bcast( h, 0, inter_bgrp_comm )
            END IF
            !
            CALL newinit( h, iverbosity )
            !
            CALL newnlinit()
            !
            CALL formf( tfirst, eself ) ! should be called after newinit ...
            !
         END IF
         !
         ! increase time step
         !
         tps = tps + delt * au_ps
         !
         call errore('driver', 'CPBO not implmentend',1)
         !
         tfirst=.FALSE. 
         !
         !DO ia=1,nat
         !  WRITE(stdout,'(3x,"fion: ",I7,3ES18.8)') nfi,fion(:,ia) 
         !END DO
         ! 
         combuf=RESHAPE(fion, (/ 3 * nat /) ) ! return force in atomic units
         ! 
         pot=etot                      ! return potential in atomic units
         vir=transpose(stress)*omega   ! return virial in atomic units and without the volume scaling
         !
         stress_gpa = stress * au_gpa
         !DO ia=1,3
         !  WRITE(stdout,'(3x,"stress2: ",I7,3F18.8)') nfi,stress_gpa(:,ia) 
         !END DO
         WRITE(stdout,'(3x,"omega: ",I7,F18.8)') nfi,omega 
         !WRITE(stdout,'(3x,"pot: ",F18.10)') pot
         !       
         hasdata=.true.
         !
         !write save file every isave step ...
         !
         IF((nfi.GT.0).AND.(MOD(nfi,isave).EQ.(isave-1)).AND.(nfi.LT.nomore)) &
           CALL writefile( h, hold, nfi, c0_bgrp, cm_bgrp, taus,  &
                           tausm, vels, velsm, acc,  lambda, lambdam, descla, xnhe0,   &
                           xnhem, vnhe, xnhp0, xnhpm, vnhp, nhpcl, nhpdim, ekincm,&
                           xnhh0, xnhhm, vnhh, velh, fion, tps, z0t, f, rhor )
         !       
      else if (trim(header)=="GETFORCE") then
         !
         if (ionode) write(*,*) "  DRIVER MODE: Returning v,forces,stress "
         if (ionode) then      
            call writebuffer(socket,"FORCEREADY  ",MSGLEN)            
            call writebuffer(socket,pot,8)
            call writebuffer(socket,nat,4)            
            call writebuffer(socket,combuf,3*nat*8)
            call writebuffer(socket,vir,9*8)
            nat=0
            call writebuffer(socket,nat,4)
         endif
         hasdata=.false.
         !
      endif
      !
      CALL stop_clock( 'driver_md' )
      !
    END DO driver_loop    
    !
    ! writefile ...
    !
1   CALL writefile( h, hold, nfi, c0_bgrp, cm_bgrp, taus,  &
                    tausm, vels, velsm, acc,  lambda, lambdam, descla, xnhe0,   &
                    xnhem, vnhe, xnhp0, xnhpm, vnhp, nhpcl, nhpdim, ekincm,&
                    xnhh0, xnhhm, vnhh, velh, fion, tps, z0t, f, rhor )
    ! 
    !
    !CALL error("leaving driver routine")
    !
    !
  END SUBROUTINE
