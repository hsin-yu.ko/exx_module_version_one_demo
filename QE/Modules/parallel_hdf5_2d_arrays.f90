module parallel_hdf5_2d_arrays
#if defined(HK_HDF5_dev) || defined(HK_HDF5_write) || defined(HK_HDF5_read)
  use hdf5
  use mpi
  implicit none
  save
  ! iterator
  integer, parameter, public :: loading = 0
  integer, parameter, public :: saving = 1
  !
  !logical, parameter    :: is_debug = .true.
  logical, parameter    :: is_debug = .false.
  integer, parameter    :: memrank  = 2           ! rank in memory
  integer, parameter    :: dsetrank = 2           ! rank in dataset
  !
  integer :: mode
  character(len=256)    :: fname                  ! file name
  character(len=256)    :: dsetname               ! dataset name
  integer(hid_t)        :: file_id
  integer(hid_t)        :: dataspace
  integer(hid_t)        :: dset_id
  integer(hid_t)        :: memspace
  integer(hsize_t)      :: dimsf(dsetrank)
  integer(hsize_t)      :: dims_ch(dsetrank)
  integer(hsize_t)      :: countf(dsetrank)
  integer(hsize_t)      :: offsetf(dsetrank)
  integer(hsize_t)      :: dimsm(memrank)
  integer(hsize_t)      :: countm(memrank)
  integer(hsize_t)      :: offsetm(memrank)
  !
  integer :: error
  !
  public  :: setup, register_range, run_io, cleanup
  private :: setup_hdf5_file_info, open_hdf5_file, open_dataset_and_dataspace
  private :: setup_range_info, add_select_dataspaces
  private :: unselect_dataspaces
contains

  subroutine  setup(fname_inp, dsetname_inp, dimsf_inp, dimsm_inp,&
      &             dims_ch_inp, mode_inp)
    !driver routine
    implicit none
    character(len=256), intent(in)    :: fname_inp              ! file name
    character(len=256), intent(in)    :: dsetname_inp           ! dataset name
    integer, intent(in)               :: dimsf_inp(dsetrank)    ! dset dimensions
    integer, intent(in)               :: dimsm_inp(memrank)     ! memory dimensions
    integer, intent(in)               :: dims_ch_inp(dsetrank)  ! chunk dimensions
    integer, intent(in)               :: mode_inp
    call h5open_f(error) ! Initialize FORTRAN interface.
    call setup_hdf5_file_info(fname_inp, dsetname_inp, dimsf_inp, dimsm_inp,&
      &                       dims_ch_inp, mode_inp)
    call open_hdf5_file
    call open_dataset_and_dataspace
    return
  end subroutine setup

  subroutine  setup_hdf5_file_info(fname_inp, dsetname_inp, dimsf_inp, dimsm_inp,&
      &                            dims_ch_inp, mode_inp)
    implicit none
    character(len=256), intent(in)    :: fname_inp              ! file name
    character(len=256), intent(in)    :: dsetname_inp           ! dataset name
    integer, intent(in)               :: dimsf_inp(dsetrank)    ! dset dimensions
    integer, intent(in)               :: dimsm_inp(memrank)     ! memory dimensions
    integer, intent(in)               :: dims_ch_inp(dsetrank)  ! chunk dimensions
    integer, intent(in)               :: mode_inp
    mode     = mode_inp
    fname    = fname_inp
    dsetname = dsetname_inp
    dimsf    = dimsf_inp
    dimsm    = dimsm_inp
    dims_ch  = dims_ch_inp
    if (is_debug) then
      write(*,*) "<setup_hdf5_file_info>"
      write(*,*) "mode       =", mode
      write(*,*) "fname      =", trim(adjustl(fname))
      write(*,*) "dsetname   =", trim(adjustl(dsetname))
      write(*,*) "dimsf      =", dimsf
      write(*,*) "dimsm      =", dimsm
      write(*,*) "dims_ch    =", dims_ch
      write(*,*) "</setup_hdf5_file_info>"
    end if
    return
  end subroutine setup_hdf5_file_info

  subroutine  open_hdf5_file()
    implicit none
    integer(hid_t)        :: plist_id
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, error)
    call h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL, error)
    if (mode.eq.loading) then
      call h5fopen_f(adjustl(trim(fname)),   H5F_ACC_RDWR_F,  file_id, error,&
        &            access_prp = plist_id)
    else if (mode.eq.saving) then
      call h5fcreate_f(adjustl(trim(fname)), H5F_ACC_TRUNC_F, file_id, error,&
        &            access_prp = plist_id)
    end if
    call h5pclose_f(plist_id, error)
    return
  end subroutine open_hdf5_file

  subroutine  open_dataset_and_dataspace
    implicit none
    integer(hid_t)        :: plist_id
    if (mode.eq.loading) then
      ! open dataset
      call h5dopen_f(file_id, adjustl(trim(dsetname)), dset_id, error)
      ! get dataspace
      call h5dget_space_f(dset_id, dataspace, error)             ! dataset dataspace
    else if (mode.eq.saving) then
      ! creat dataspace
      call h5screate_simple_f(dsetrank, dimsf, dataspace, error) ! dataset dataspace
      ! creat dataset
      call h5pcreate_f(H5P_DATASET_CREATE_F, plist_id, error)    ! dataset property
      call h5pset_chunk_f(plist_id, dsetrank, dims_ch, error)    ! dataset chunking
      call h5dcreate_f(file_id, adjustl(trim(dsetname)), H5T_NATIVE_DOUBLE, &
        &              dataspace, dset_id, error, plist_id)
      call h5pclose_f(plist_id, error) ! close properties
    end if
    call h5screate_simple_f(memrank,  dimsm, memspace,  error) ! creat memory dataspace
    return
  end subroutine open_dataset_and_dataspace

  subroutine  register_range(countf_inp, offsetf_inp,&
      &                      countm_inp, offsetm_inp, override)
    ! driver routine
    implicit none
    integer, intent(in)   :: countf_inp(dsetrank)   ! hyperslab size
    integer, intent(in)   :: offsetf_inp(dsetrank)  ! offset in file
    integer, intent(in)   :: countm_inp(memrank)    ! hyperslab size (memory)
    integer, intent(in)   :: offsetm_inp(memrank)   ! offset in file (memory)
    logical, intent(in)   :: override
    call setup_range_info(countf_inp, offsetf_inp, countm_inp, offsetm_inp)
    call add_select_dataspaces(override)
    return
  end subroutine register_range

  subroutine  setup_range_info(countf_inp, offsetf_inp,&
      &                          countm_inp, offsetm_inp)
    implicit none
    integer, intent(in)   :: countf_inp(dsetrank)   ! hyperslab size
    integer, intent(in)   :: offsetf_inp(dsetrank)  ! offset in file
    integer, intent(in)   :: countm_inp(memrank)    ! hyperslab size (memory)
    integer, intent(in)   :: offsetm_inp(memrank)   ! offset in file (memory)
    countf  = countf_inp
    offsetf = offsetf_inp
    countm  = countm_inp
    offsetm = offsetm_inp
    if (is_debug) then
      write(*,*) "<setup_range_info>"
      write(*,*) "countf  =", countf
      write(*,*) "offsetf =", offsetf
      write(*,*) "countm  =", countm
      write(*,*) "offsetm =", offsetm
      write(*,*) "</setup_range_info>"
    end if
    return
  end subroutine setup_range_info

  subroutine  add_select_dataspaces(override)
    implicit none
    logical, intent(in)   :: override
    logical               :: is_non_selection
    is_non_selection = (product(countm).eq.0)
    if (override) then
      call h5sselect_hyperslab_f(dataspace, H5S_SELECT_SET_F, offsetf, countf, error)
      call h5sselect_hyperslab_f(memspace,  H5S_SELECT_SET_F, offsetm, countm, error)
    else
      call h5sselect_hyperslab_f(dataspace, H5S_SELECT_OR_F, offsetf, countf, error)
      call h5sselect_hyperslab_f(memspace,  H5S_SELECT_OR_F, offsetm, countm, error)
    end if
    if (is_non_selection) then
      call h5sselect_none_f(dataspace,error)
      call h5sselect_none_f(memspace,error)
    end if
    return
  end subroutine add_select_dataspaces

  subroutine  run_io(dimsm_inp, var)
    !driver routine
    implicit none
    integer, intent(in)   :: dimsm_inp(memrank)             ! dset dimensions (memory)
    real(8), intent(out)  :: var(dimsm_inp(1),dimsm_inp(2)) ! variable to save
    integer(hid_t)        :: plist_id
    call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)
    call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
    !call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_INDEPENDENT_F, error)
    if (mode.eq.loading) then
      call h5dread_f (dset_id, H5T_NATIVE_DOUBLE, var, dimsm, error,&
        &             memspace, dataspace, xfer_prp=plist_id)
    else if (mode.eq.saving) then
      call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, var, dimsm, error,&
        &             memspace, dataspace, xfer_prp=plist_id)
    end if ! mode
    call h5pclose_f(plist_id, error)
    return
  end subroutine run_io

  subroutine  cleanup()
    !driver routine
    implicit none
    call unselect_dataspaces
    call h5sclose_f(memspace, error)
    call h5sclose_f(dataspace, error)
    call h5dclose_f(dset_id, error)
    call h5fclose_f(file_id, error)
    call h5close_f(error)
    return
  end subroutine cleanup

  subroutine  unselect_dataspaces()
    implicit none
    call h5sselect_none_f(dataspace,error)
    call h5sselect_none_f(memspace,error)
    return
  end subroutine unselect_dataspaces

#endif
end module parallel_hdf5_2d_arrays
