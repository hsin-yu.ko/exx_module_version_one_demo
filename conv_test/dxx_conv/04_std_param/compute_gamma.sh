#!/bin/bash
refd=../01_print_ref_dxx/
off=5000
nbnd=256
fout=gamma.dat

rm -f $fout
for i in `seq $nbnd`
do
  n=$[off+i]
  fref=$refd/fort.$n
  ftgt=./fort.$n
  paste $ftgt $fref | awk -v i=$i '{d=($1-$2)}{sad+=sqrt(d*d)}{sa+=sqrt($2*$2)}END{print i, sad/sa}' >> $fout
done
