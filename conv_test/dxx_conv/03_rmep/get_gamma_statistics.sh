#!/bin/bash

rm -f gamma_stat.dat

for i in `ls -d ./rme*/`
do
  cp util_stat_gamma.m $i
  cd $i
  octave util_stat_gamma.m | grep -v ans|grep -v '^$' | awk -v d=$i '{print d,$0}' >> ../gamma_stat.dat
  cd ..
done
