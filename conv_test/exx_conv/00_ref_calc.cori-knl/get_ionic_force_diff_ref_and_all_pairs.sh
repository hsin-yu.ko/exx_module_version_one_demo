awk '{d=$1-$4}{dx=sqrt(d*d)}{d=$2-$5}{dy=sqrt(d*d)}{d=$3-$6}{dz=sqrt(d*d)}{print dx,dy,dz}' ./ionic_forces_default_and_ref.dat | xargs -n 1| sort -g  > dforces_default_and_ref.dat
awk '{d=$1-$4}{dx=sqrt(d*d)}{d=$2-$5}{dy=sqrt(d*d)}{d=$3-$6}{dz=sqrt(d*d)}{print dx,dy,dz}' ./ionic_forces_ref_and_all_pairs.dat | xargs -n 1| sort -g  > dforces_ref_and_all_pairs.dat

