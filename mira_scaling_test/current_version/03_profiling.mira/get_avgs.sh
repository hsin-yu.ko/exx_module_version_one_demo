#!/bin/bash
for i in `ls -d w*/`;do echo $i; cd $i; ./gather_info.sh ; ./gather_info-additional-gga.sh ;cd .. ;done

rm -fr statistics
mkdir -p statistics

paste w064.rep[1-3]/MPI0128/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w064-z0.5.avg
paste w064.rep[1-3]/MPI0256/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w064-z1.avg
paste w064.rep[1-3]/MPI0512/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w064-z2.avg
paste w064.rep[1-3]/MPI1024/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w064-z4.avg
paste w128.rep[1-3]/MPI0256/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w128-z0.5.avg
paste w128.rep[1-3]/MPI0512/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w128-z1.avg
paste w128.rep[1-3]/MPI1024/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w128-z2.avg
paste w128.rep[1-3]/MPI2048/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w128-z4.avg
paste w256.rep[1-3]/MPI0512/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w256-z0.5.avg
paste w256.rep[1-3]/MPI1024/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w256-z1.avg
paste w256.rep[1-3]/MPI2048/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w256-z2.avg
paste w256.rep[1-3]/MPI4096/timings.avg| awk '{print $1,($2+$5+$8)/3}' > statistics/timings-w256-z4.avg

cat w064.rep[1-3]/MPI0128/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w064-z0.5.avg.crt_acc
cat w064.rep[1-3]/MPI0256/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w064-z1.avg.crt_acc
cat w064.rep[1-3]/MPI0512/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w064-z2.avg.crt_acc
cat w064.rep[1-3]/MPI1024/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w064-z4.avg.crt_acc
cat w128.rep[1-3]/MPI0256/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w128-z0.5.avg.crt_acc
cat w128.rep[1-3]/MPI0512/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w128-z1.avg.crt_acc
cat w128.rep[1-3]/MPI1024/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w128-z2.avg.crt_acc
cat w128.rep[1-3]/MPI2048/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w128-z4.avg.crt_acc
cat w256.rep[1-3]/MPI0512/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w256-z0.5.avg.crt_acc
cat w256.rep[1-3]/MPI1024/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w256-z1.avg.crt_acc
cat w256.rep[1-3]/MPI2048/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w256-z2.avg.crt_acc
cat w256.rep[1-3]/MPI4096/timings.avg.crt_acc | awk '(NR==1){print $0}(NR%2==0){{tcomp+=$1}{fcomp+=$2}{tcomm+=$3}{fcomm+=$4}{tidle+=$5}{fidle+=$6}{texx+=$7}}END{print tcomp/3,fcomp/3,tcomm/3,fcomm/3,tidle/3,fidle/3,texx/3}'| column -t  > statistics/timings-w256-z4.avg.crt_acc


cat w064.rep[1-3]/MPI0128/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w064-z0.5.pbe
cat w064.rep[1-3]/MPI0256/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w064-z1.pbe
cat w064.rep[1-3]/MPI0512/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w064-z2.pbe
cat w064.rep[1-3]/MPI1024/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w064-z4.pbe
cat w128.rep[1-3]/MPI0256/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w128-z0.5.pbe
#cat w128.rep[1-3]/MPI0512/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w128-z1.pbe
cat w128.rep[1-3]/MPI1024/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w128-z2.pbe
cat w128.rep[1-3]/MPI2048/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w128-z4.pbe
# cat w256.rep[1-3]/MPI0512/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w256-z0.5.pbe
# cat w256.rep[1-3]/MPI1024/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w256-z1.pbe
cat w256.rep[1-3]/MPI2048/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w256-z2.pbe
cat w256.rep[1-3]/MPI4096/timings.pbe |awk '{s+=$8}END{print s/NR}' > statistics/timings-w256-z4.pbe
#
cat w128.rep[1-3]/MPI0512.gga-only/timings.pbe |awk '{s+=$1}END{print s/NR}' > statistics/timings-w128-z1.pbe
cat w256.rep[1-3]/MPI0512.gga-only/timings.pbe |awk '{s+=$1}END{print s/NR}' > statistics/timings-w256-z0.5.pbe
cat w256.rep[1-3]/MPI1024.gga-only/timings.pbe |awk '{s+=$1}END{print s/NR}' > statistics/timings-w256-z1.pbe


cat w064.rep[1-3]/MPI0128/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w064-z0.5.mlwf
cat w064.rep[1-3]/MPI0256/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w064-z1.mlwf
cat w064.rep[1-3]/MPI0512/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w064-z2.mlwf
cat w064.rep[1-3]/MPI1024/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w064-z4.mlwf
cat w128.rep[1-3]/MPI0256/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w128-z0.5.mlwf
cat w128.rep[1-3]/MPI0512/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w128-z1.mlwf
cat w128.rep[1-3]/MPI1024/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w128-z2.mlwf
cat w128.rep[1-3]/MPI2048/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w128-z4.mlwf
cat w256.rep[1-3]/MPI0512/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w256-z0.5.mlwf
cat w256.rep[1-3]/MPI1024/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w256-z1.mlwf
cat w256.rep[1-3]/MPI2048/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w256-z2.mlwf
cat w256.rep[1-3]/MPI4096/mlwf_summary.dat |awk '{s+=$2}END{print s/NR}' > statistics/timings-w256-z4.mlwf
