#!/bin/bash

# JOB INFO
# parent block : 1k nodes 

# path for cp.x executable...
CP=/gpfs/mira-fs1/projects/CrystalsADSP/hsinyu/exx_module_version_one_demo/QE/CPV/src/cp.x_profile_hdf5.exx_03_barrier.no_io

for i in `seq 6`
do

pushd ./w256.rep${i}/MPI1024/
runjob -n 1024 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 1 -ndiag 1024  < ./input.vc.1s > output.vc
popd

pushd ./w256.rep${i}/MPI1024.gga-only/
runjob -n 1024 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 4 -ndiag 1024 < ./input.vc.1s  > output.vc 
popd

pushd ./w128.rep${i}/MPI1024/
runjob -n 1024 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 4 -ndiag 512  < ./input.vc.1s > output.vc 
popd

pushd ./w064.rep${i}/MPI1024/
runjob -n 1024 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 4 -ndiag 256  < ./input.vc.1s >> output.vc 
popd

done
